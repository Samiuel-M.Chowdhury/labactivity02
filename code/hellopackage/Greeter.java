package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;


public class Greeter{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        // java.util.Random rand = new java.util.Random();
        Random random = new Random();
        Utilities utily = new Utilities();

        System.out.println("Enter an integer value");
        int userNumber = scan.nextInt(); 
        userNumber=utily.doubleMe(userNumber);
        System.out.println("Doubled number: "+userNumber);
    }
}
